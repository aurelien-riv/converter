var VolumeConverter = {
    init: function() {
        var self = this;

        $('[data-galuk]').on('change, keyup', function(evt) {
            var gal = eval($(this).data('galuk'));
            var ukval  = parseFloat($(this).val()) * gal;

            self.updateUK(ukval);
            self.updateL(ukval * 4.54609);
            self.updateUS(ukval * 1.20095042);
        });
        $('[data-galus]').on('change, keyup', function(evt) {
            var gal = eval($(this).data('galus'));
            var usval  = parseFloat($(this).val()) * gal;

            self.updateUS(usval);
            self.updateUK(usval / 1.20095042);
            self.updateL(usval * 3.7854);
        });
        $('[data-litre]').on('change, keyup', function(evt) {
            var litre = eval($(this).data('litre'));
            var lval  = parseFloat($(this).val()) * litre;

            self.updateL(lval);
            self.updateUK(lval / 4.54609);
            self.updateUS(lval / 3.7854);
        });
    },
    updateUK: function(value) {
        this.updateAny('galuk', value);
    },
    updateUS: function(value) {
        this.updateAny('galus', value);
    },
    updateL: function(value) {
        this.updateAny('litre', value);
    },
    updateAny: function (dataAttr, value) {
        $('[data-'+dataAttr+'=1]').val(value);
        $('[data-'+dataAttr+'][data-'+dataAttr+'!=1]').each(function (i, e) {
            $(e).val(value / eval($(e).data(dataAttr)));
        });
    }
};
VolumeConverter.init();
