var CurrencyConverter = {
    frais_fixe_retrait: undefined,
    taux_retrait: undefined,
    frais_fixe_cb: undefined,
    taux_cb: undefined,
    taux_euro_livre: undefined,

    init: function() {
        var self = this;

        if (typeof localStorage.currencyconverter === 'undefined')
            localStorage.currencyconverter = JSON.stringify({});
        else
            this.loadParams();

        $('#currency_settings_content_2').on('change', ':input', function() {
            self.updateParams();
        }).find(':input').first().trigger('change');

        $('#valeur_euro').on('change, keyup', function() {
            var val = parseFloat($(this).val());
            $('#valeur_livre').val(val * self.taux_euro_livre);
            self.updateFrais();
        });
        $('#valeur_livre').on('change, keyup', function() {
        console.log(self);
            var val = parseFloat($(this).val());
            $('#valeur_euro').val((val * (1/self.taux_euro_livre)).toFixed(2));
            self.updateFrais();
        });
    },
    loadParams: function () {
        var params = JSON.parse(localStorage.currencyconverter);
        ['frais_fixe_retrait', 'taux_retrait', 'frais_fixe_cb', 'taux_cb', 'taux_euro_livre'].forEach(function (val, i, array) {
            $('#'+val).val(params[val]);
        });
    },
    updateParams: function () {
        var params = JSON.parse(localStorage.currencyconverter);
        for (var val of ['frais_fixe_retrait', 'taux_retrait', 'frais_fixe_cb', 'taux_cb', 'taux_euro_livre']) {
            this[val] = parseFloat($('#'+val).val());
            params[val] = this[val];
        }
        localStorage.currencyconverter = JSON.stringify(params);
    },
    updateFrais: function() {
        var val_euro = parseFloat($('#valeur_euro').val());
        $('#frais_cb')     .val((val_euro * this.getTauxCB()      + this.frais_fixe_cb).toFixed(2));
        $('#frais_retrait').val((val_euro * this.getTauxRetrait() + this.frais_fixe_retrait).toFixed(2));
    },
    getTauxCB: function() {
        return this.taux_cb / 100;
    },
    getTauxRetrait: function() {
        return this.taux_retrait / 100;
    }
};
CurrencyConverter.init();
