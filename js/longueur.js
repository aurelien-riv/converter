var LongueurConverter = {
    init: function() {
        var self = this;

        $('[data-foot]').on('change, keyup', function(evt) {
            var foot = eval($(this).data('foot'));
            var val  = parseFloat($(this).val()) * foot;
            self.updateGB(val);
            self.updateM(val * 0.3048);
        });
        $('[data-metre]').on('change, keyup', function(evt) {
            var metre = eval($(this).data('metre'));
            var val  = parseFloat($(this).val()) * metre;
            self.updateM(val);
            self.updateGB(val / 0.3048);
        });
    },
    updateGB: function(value) {
        this.updateAny('foot', value);
    },
    updateM: function(value) {
        this.updateAny('metre', value);
    },
    updateAny: function (dataAttr, value) {
        $('[data-'+dataAttr+'=1]').val(value);
        $('[data-'+dataAttr+'][data-'+dataAttr+'!=1]').each(function (i, e) {
            $(e).val(value / eval($(e).data(dataAttr)));
        });
    }
};
LongueurConverter.init();
