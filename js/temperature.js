var TemperatureConverter = {
    init: function() {
        $('#valeur_celcius').on('change, keyup', function() {
            var val = parseFloat($(this).val());
            $('#valeur_fahrenheit').val(val * 9/5 + 32);
        });
        $('#valeur_fahrenheit').on('change, keyup', function() {
            var val = parseFloat($(this).val());
            $('#valeur_celcius').val(((val - 32) * 5/9).toFixed(2));
        });
    }
};
TemperatureConverter.init();
